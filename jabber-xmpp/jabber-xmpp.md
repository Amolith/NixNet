---
layout: page
title: Jabber/XMPP
description: "Set up a Jabber/XMPP account on NixNet.xyz"
cover: /assets/pages/xmpp.png
permalink: /jabber-xmpp/
---
# Registration - clearnet
Client registration is open so all you have to do to start using Jabber/XMPP on NixNet is open your favourite client, enter `nixnet.xyz` as the host, come up with a username, generate a strong password, and enjoy!  

# Registration - Tor
If you want to use my XMPP server over Tor, make sure you first start/enable the Tor service:
```bash
$ sudo systemtl start tor.service
```
After that, there are a few settings you'll have to change depending on your client. I use [Gajim](https://gajim.org) on Linux so I'll explain how to do it with that.

* `Add Account -> I want to register for a new account`
* Server: `nixnet.xyz`
* Advanced
  * Proxy: `Tor`
  * Manage `->` Tor
    * Type: `SOCKS5`
    * Proxy Host: `127.0.0.1`
    * Proxy Port: `9050`
  * ☑️ Use  custom hostname/port
    * Hostname: `l4qlywnpwqsluw65ts7md3khrivpirse744un3x7mlskqauz5pyuzgqd.onion`
    * Port: `5222`
* Enter a username
* Come up with a strong password - it won't be accepted if it's too weak ¯\\_(ツ )_/¯
* Enjoy! ☺️

---
# Clients
If you're not sure what client to use, I recommend [Conversations](https://f-droid.org/en/packages/eu.siacs.conversations/) for Android,  [AstraChat](https://itunes.apple.com/us/app/astrachat-direct-xmpp-voip/id923002139?mt=8) or [Monal IM](https://monal.im/) for iOS/macOS (I haven't tested them and AstraChat seems to be proprietary), and [Gajim](https://gajim.org/) for Linux and Windows.

Personally, I use Conversations and Gajim 😉
