---
layout: page
title: Friends of NixNet
subtitle: A list of some like-minded people and their websites
description: A list of some like-minded people and their websites
cover: /assets/pages/friends.png 
permalink: /friends/
---
## People
* [Anxhelo Lushka](https://lushka.al/) - graphic designer, frontend developer, computer engineering student and heavy open source user/advocate. Has an interesting blog.
* [Luke Smith](http://lukesmith.xyz/) - makes technology videos on YouTube, is the Luke of [Luke's Auto-Rice Bootstrapping Scripts (LARBS)](https://larbs.xyz/), and does a podcast called _Not Related_ on "Big-Braned" topics

## Organisations
* [LibreHo.st](https://libreho.st/) - a network of average people who host FLOSS services available for anyone to use
* [Lelux.fi](https://lelux.fi/) - really cool guy who hosts some services like I do
* [Snopyta.org](https://snopyta.org/) - Snopyta provides online services based on freedom, privacy and decentralization. Part of LibreHosters.
* [LibreDesigners.org](https://libredesigners.org/) - a website for learning how to create art, interfaces, websites, etc. with free/libre and open source software (Portuguese-only right now)
* [CYGONetwork.com](https://cygonetwork.com) - alternative to Facebook. Better features, better community, and enhanced privacy/security
