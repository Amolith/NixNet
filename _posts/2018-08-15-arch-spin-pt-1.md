---
layout: post
title: Arch spin pt. 1 - The perfect bootable
subtitle: "There's no such thing . . . yet"
description: I started trying to think of a distro that fit all my daily needs that I could take on a flash drive with me wherever I went and . . . I couldn't.
cover: /assets/arch-spin/pt-1.png
categories: arch-spin
date: 2018-8-15 10:38
---
# Thus begins the journey
Today I had an orthodontist appointment and a voice lesson. The appointment was at 15:00 and the voice lesson at 16:00. I got to the orthodontist's office 10 minutes early, was told to right to the back (like always), I sat down in the seat, and the orthodontist came over after a few minutes with another patient. He looked at my teeth, had me put my retainers in, checked how they fit, then said I don't ever need to come back (unless I do need to). That all took about 7 or 8 minutes. My voice lesson was ~5 minutes away so I had an hour to kill. I drove to the college (where the lesson was), went into the computer lab, and booted my [multibootable](http://multibootusb.org/) bootable.

I went through the distros I had and chose the [i3 spin of Manjaro](https://manjaro.org/category/community-editions/i3/), forgetting that it hadn't written correctly and was corrupt. I went through a couple of other distros that were as well and settled on [Parrot Home](https://www.parrotsec.org/download-home.php). While I love Parrot Home for security reasons, it wasn't what I was looking for. I started trying to think of a distro that fit all my daily needs that I could take on a flash drive with me wherever I went and . . . I couldn't.

I would boot it, try to install some app I'm missing (Telegram, for instance), find that I need to first update everything then upgrade some packages then have no space left to install Telegram. There isn't one distro I can think of that I wouldn't have to do that with. So I thought I'd try my hand at installing Arch on a flash drive.

As I was reading, I decided I would rather make a *live* system. This way, I can log into whatever I need to and, as soon as I turn it off, whatever I did disappears. I asked around in the Arch [Telegram channel](https://t.me/archlinuxgroup) and was given a few pages to read up on the wiki as well as a youtube video. First is [building the arch iso](https://wiki.archlinux.org/index.php/archiso), [making a custom repo](https://wiki.archlinux.org/index.php/Pacman/Tips_and_tricks#Custom_local_repository) for installing AUR packages, building them in a [chroot](https://wiki.archlinux.org/index.php/DeveloperWiki:Building_in_a_Clean_Chroot) so you don't mess with your current setup, and the [YouTube videos](https://www.youtube.com/watch?v=DqV1BJtJXEA) that help tie it all together:

# Summary
That'll be it for this post. It was originally a lot longer but I think I want to keep them to a quick read so it's easier to pick up where you left off. I'm not sure what the next post will contain but I am sure that it talks about setting up your dev environment 😉
