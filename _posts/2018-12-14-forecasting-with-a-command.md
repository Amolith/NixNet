---
layout: post
title: "Forecasting with a command"
categories: minimalism
cover: /assets/weather.png
description: "Using a single command to get the week's weather forecast"
date: 2018-12-14 21:41
---
We all love the CLI *(and if we don't, we should)* so wouldn't it be great if there was a tool to see your weather forecast with a single command? Ceda EI at <a href="https://webionite.com" target="_blank">webionite.com</a> has written a tool that uses the Dark Sky API to give you the forecast for a day or week, at your location, and in Celsius or Fahrenheit.

## Celsius
+ Weather for one week - `$ curl sky.webionite.com/location`
+ Weather for today - `$ curl sky.webionite.com/location/t`

## Fahrenheit
+ Weather for one week - `$ curl sky.webionite.com/f/location`
+ Weather for today - `$ curl sky.webionite.com/f/location/t`

A great way to make this faster and simpler is to put an alias in your shell's `rc` file. For bash, you can do that with:
```
echo "alias weather='curl sky.webionite.com/f/location'" >> ~/.bashrc
```
If you ever forget how to use the tool, just run `curl sky.webionite.com` and you'll be given instructions.
![](/assets/weather.png)
