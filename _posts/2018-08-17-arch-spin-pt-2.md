---
layout: post
title: Arch spin pt. 2  - Initial setup
subtitle: First steps to rolling your own spin
description: Setting up the basics for build your own Arch-based spin with archiso, the official development tool
categories: arch-spin
cover: /assets/arch-spin/pt-2.png
date: 2018-08-17 14:11:07
---
 *I **think** I'm going to call it a spin . . .*
1. Install the package `archiso` from the official repos or `archiso-git` from the AUR
2. `$ mkdir ~/<build-directory>`
  * Replace `<build-directory>` with wherever you want the iso build to be stored. This is where we'll be spending all of our time configuring. Mine is at `~/liveiso/` and that's the path I'll be using in this and future posts
3. `$ sudo cp -r /usr/share/archiso/configs/releng/ ~/liveiso`
4. Edit `~/liveiso/packages.x86_64` to install desired software
  * This will be addressed in the next post, `packages.x86_64`, where I also give some quick ways to install everything you might want.
