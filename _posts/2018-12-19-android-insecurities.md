---
layout: post
title: Android's Insecurities
subtitle: Exploring this terrible OS from G00gl3
description: A semi in-depth look at some of the ways Android is a terrible operating system from a privacy, security, and FLOSS perspective
cover: /assets/spydroid.png
categories: [android,security]
date: 2018-12-19 00:53 -0500
---
# <center>This post is a work in progress.</center>
## <center>For now, I am simply storing some links to use while writing it.</center>
---
### The Verge
* [Google remotely changed the settings on a bunch of phones running Android 9 Pie](https://www.theverge.com/2018/9/14/17861150/google-battery-saver-android-9-pie-remote-settings-change)
* [Google personalizes search results even when you’re logged out, new study claims](https://www.theverge.com/2018/12/4/18124718/google-search-results-personalized-unique-duckduckgo-filter-bubble)

### Stallman
* [Reasons not to use Google - Surveillance](https://stallman.org/google.html#surveillance)

### GNU
* [Android and Users' Freedom](https://www.gnu.org/philosophy/android-and-users-freedom)

### YouTube
* [How much info is Google getting from your phone?](https://invidio.us/watch?v=0s8ZG6HuLrU)

### Misc.
* [Cambridge - Everything this app collects can be collected by any other app](https://deviceanalyzer.cl.cam.ac.uk/collected.htm)
