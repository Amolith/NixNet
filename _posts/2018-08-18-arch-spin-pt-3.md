---
layout: post
title: Arch spin pt. 3 - packages.x86_64
description: Getting all the packages you currently have installed from the Arch repos installed to your Arch ISO
categories: arch-spin
cover: /assets/arch-spin/pt-3.png
date: 2018-08-18 16:52:42
---
# Package configuration
As I said in the last post, this is the file in which you list the applications you wish to install. I won't list the defaults because there a lot. *However*, this basic setup only builds to a total of ~440 MB so you can add *many* more applications.

I want the setup on this bootable to be exactly the same as what I currently have on my system. I did not want to manually enter every single package though. Thankfully, pacman is a feature-complete tool and it lets you get a *lot* of information about installed packages. `pacman -Qne` lists all the currently installed packages that you explicitly installed. It does not list dependencies of those applications. What I did was run `pacman -Qne >> ~/liveiso/packages.x86_64` to add all the apps I have installed from the official Arch repos to the end of the file so nothing was overwritten.

After, I ran `pacman -Qni >> official.txt` so I could get information about all those packages and decide whether or not I wanted to keep them. For example, I removed some stuff from deepin that I no longer used, SuperTuxKart, and a lot of other stuff. This shrunk my iso from 3.6 GB to 2.5. Now I have a lot of space to use for installing my applications from the AUR. This includes [making a custom repo](https://wiki.archlinux.org/index.php/Pacman/Tips_and_tricks#Custom_local_repository) for the packages and [building them in a chroot](https://wiki.archlinux.org/index.php/DeveloperWiki:Building_in_a_Clean_Chroot) so you make *sure* you have all the needed dependencies while keeping your system from being messed up while building.

All of this will be discussed in the next blog post (when I get to it).
