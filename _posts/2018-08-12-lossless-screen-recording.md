---
layout: post
title: Lossless screen recording
subtitle: Never waste resources with OBS again
description: Recording your screen (or monitors) with ffmpeg for a high-quality lossless video that uses very little system resources
categories: minimalism
cover: /assets/ffmpeg-lossless.png
date: 2018-08-12 17:15:20
---
I've been trying off and on for the past few weeks to figure out how to record my 1920x1080 monitor. The recording is going to be some music videos for a friend. Originally, it was just going to be a single background image for the whole video then I had the idea of using [cava](https://github.com/karlstav/cava) in a transparent terminal on top of the background. This didn't work at all because it actually kept freezing when I tried to record it. So I tried switching to [ncmpcpp](http://ncmpcpp.rybczak.net/)'s visualiser. This still had horrible lag so I've been puzzling over how to use ffmpeg to *losslessly* record my second monitor. The reason OBS and similar screen recorders are so slow is because, most of the time, they encode to the end format while recording and that uses a lot of system resources. I finally figured it out and have pasted the command below.

`ffmpeg -video_size 1920x1080 -framerate 30 -f x11grab -draw_mouse 0 -i :0.0+1366,0 -c:v libx264 -crf 0 -preset ultrafast output.mkv`

Above is exactly what I used for my 1080p monitor with 768p laptop screen. I've modified the command so you can see what you need to edit for your use-case.

`ffmpeg -video_size <target-resolution> -framerate 30 -f x11grab -i :0.0+<width-of-unused-monitor>,0 -c:v libx264 -crf 0 -preset ultrafast <filename>.mkv`

If you do *not* want the cursor recorded, add `-draw_mouse 0` directly after `x11grab` like I did in the first command.

My video was 470mb for a ~13 minute video. If you're going to archive the recording or are concerned about file size, re-encode it with a slower preset. This will be a lot slower and take a lot of CPU but the resulting file is *significantly* smaller than the original and still lossless. I this as a general purpose screen recorder. Previously, I was using OBS and the lag in the video was incredible but with ffmpeg, it's smooth as butter. The command for re-encoding is below:

`ffmpeg -i output.mkv -c:v libx264 -crf 0 -preset veryslow output-smaller.mkv`
![](/assets/ffmpeg-lossless.gif)

# <center>EDIT:</center>
This command only works with X, not Wayland. Skimming `ffmpeg`'s man page, I see that `video4linux2` is another option for capturing video so you may be able to replace `x11grab` with it for the same result. I have not tested this so I don't know if it'll work or not.
