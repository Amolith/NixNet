---
layout: post
title: Removing Masto usernames from PostgreSQL
subtitle: Give a Mastodonian their handle back
description: Give a Mastodonian their handle back
cover: /assets/posts/mastodon.png
date: 2019-01-30 13:16 -0500
---
The other day, I had a friend of mine create an account on [my Mastodon instance](https://masto.nixnet.xyz/). I made him an admin account thinking I'd have the _ability_ to demote him if I needed to only to find that I couldn't. So I had him delete his account thinking that he'd be able to remake it with the same username only to find that he couldn't. This is the username he uses everywhere (like me with Amolith) so I wanted to get it back for him. I did some digging and found that I would have to delete the handle from Masto's PostgreSQL database. I'd never used postgres before so it was very new; a rundown of the basic commands is below along with the one I used to delete his username at the end.

***NOTE:*** I don't know how this will affect your instance if the user had toots before they deleted their account. Handles federate from instance to instance and things could very well break if another instance had the user's information already. Thankfully, my friend was busy that day and hadn't even started configuring his account, much less tooted anything.

# PostgreSQL Syntax
`\l` - list databases  
`\c <database>` - change databases  
`\d` - list tables  
`\d+ <table>` - list columns in the specified table  
`select <column> from <table>;` - show the entries in the specified column  
`delete from <table> where <column> = '<user_handle>';` - delete the username  

The command sequence I used was:  
`\c mastodon`  
`delete from accounts where username='<handle>';`
