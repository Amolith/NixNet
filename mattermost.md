---
layout: page
title: Mattermost
subtitle: "An open source and self-hosted chat application"
description: "A list of communities and a NixNet-styled theme for Mattermost"
cover: /assets/pages/mattermost.png 
---
# Communities
---
Sign up by clicking a community link
- [NixNet](https://matter.nixnet.xyz/signup_user_complete/?id=n5qbfgiuaidmtmppetgj45zxuh) - General chat, randomness, shiptosting, getting support, etc.
- [Linux Audio](https://matter.nixnet.xyz/signup_user_complete/?id=5ob84if1w3bx3n5phhsbjm3ohh) - Open to anyone with an interest in audio recording, production, etc. on Linux
- [LenoirLUG](https://matter.nixnet.xyz/signup_user_complete/?id=rrko34wdzjfo5ykc49d1kkikeo) - A chat room for the Lenoir Linux User Group

# Mobile client config
To use the mobile application, first sign up on a community with your browser (desktop or mobile) then enter `https://matter.nixnet.xyz` as the server URL! Desktop themes can be synced to mobile if you want to use the theme I created below.

# Theme
---
I've created a theme for Mattermost that uses the same colour scheme as the NixNet website. Copy and paste the code below into the application's theme settings under `Account Settings > Display > Theme > Custom Theme`.
```json
{
  "sidebarBg":"#242424",
  "sidebarText":"#ffffff",
  "sidebarUnreadText":"#ffffff",
  "sidebarTextHoverBg":"#525252",
  "sidebarTextActiveBorder":"#2b6863",
  "sidebarTextActiveColor":"#ffffff",
  "sidebarHeaderBg":"#242424",
  "sidebarHeaderTextColor":"#ffffff",
  "onlineIndicator":"#2b6863",
  "awayIndicator":"#68652b",
  "dndIndicator":"#682b2b",
  "mentionBj":"#573c3c",
  "mentionColor":"#ffffff",
  "centerChannelBg":"#333333",
  "centerChannelColor":"#ffffff",
  "newMessageSeparator":"#2b6863",
  "linkColor":"#5a76c7",
  "buttonBg":"#5a76c7",
  "buttonColor":"#ffffff",
  "errorTextColor":"#654040",
  "mentionHighlightBg":"#949494",
  "mentionHighlightLink":"#66aaa5",
  "codeTheme":"monokai",
  "mentionBg":"#573c3c"
}
```
